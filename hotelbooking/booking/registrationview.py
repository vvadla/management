from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from rest_framework.parsers import JSONParser
from booking.views import UserPermissionList
from booking.models import Userpermissions, Roles
from rest_framework.response import Response
from django.http import Http404
from rest_framework import status



@csrf_exempt
def user_list(request):
    """
    List all code users, or create a new user.
    """
    if request.method == 'GET':
        users = User.objects.all()
        qs_json = serializers.serialize('json', users)
        return HttpResponse(qs_json, content_type='application/json')

    elif request.method == 'POST':
        try:
            data = JSONParser().parse(request)
            user = User.objects.create_user(data["username"], data["email"], data["password"])
            for i in range(len(data['role'])):
                role = Roles.objects.get(name=data['role'][i])
                user_permission = Userpermissions(role=role,user=user)
                print(user_permission)
                user_permission.save()
            return JsonResponse([{"message":"user is created","userid":user.id,
                                "permissions_granted  as":data['role']}],safe = False)
        except Exception as e:
            return JsonResponse({"error":"{0}".format(e)})
