from booking.models import Userpermissions
from rest_framework import serializers


class UserPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Userpermissions
        fields = ['id', 'user','role']