from django.shortcuts import render

# Create your views here.
from booking.models import Roles,Userpermissions
from booking.roles_serializer import RoleSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from booking.userpermission_serializer import UserPermissionSerializer

class RolesList(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        role = Roles.objects.all()
        serializer = RoleSerializer(role, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = RoleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserPermissionList(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        permission = Userpermissions.objects.all()
        serializer = UserPermissionSerializer(permission, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        print(request.data)
        serializer = UserPermissionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)