from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from booking.models import Userpermissions, Roles
import jwt


@csrf_exempt
@api_view(['POST'])
def login(request):
    """
    List all code users, or create a new user.
    """
    if request.method == 'POST':
        data = request.data
        if "username" and "password" in data.keys():
            user = authenticate(username=data['username'], password=data["password"])
            output_json = {}
            if user is not None:
                permissions = list(Userpermissions.objects.filter(user_id=user.id).select_related('role').values_list('role__name',flat=True))
                payload = {'id': user.id,'email': user.email}
                jwt_token = {'token': jwt.encode(payload, "SECRET_KEY"),"permissions":permissions}
                return HttpResponse(json.dumps(jwt_token),status=200,content_type="application/json")
            else:
                return Response(json.dumps({'Error': "Invalid credentials"}),status=400,content_type="application/json")
            # else:
            #     return JsonResponse({"data":"No user found in the database"})
            return JsonResponse({"data":output_json})
        else:
            return JsonResponse({"data":"username or password is missing."})
