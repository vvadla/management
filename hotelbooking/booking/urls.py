from django.urls import path
from booking import views
from booking import registrationview
from booking import loginview

urlpatterns = [
    path('roles/', views.RolesList.as_view()),
    path('signup/',registrationview.user_list),
    path('permissions/',views.UserPermissionList.as_view()),
    path('login/',loginview.login),
    # path('snippets/<int:pk>/', views.snippet_detail.as_view()),
]