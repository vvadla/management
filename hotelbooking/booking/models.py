from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Roles(models.Model):
    name = models.CharField(max_length=100, blank=True, default='')
    code = models.IntegerField(blank=False,null=False)

    class Meta:
        managed = True
        ordering = ['code']

class Userpermissions(models.Model):
    user = models.ForeignKey(User,on_delete = models.CASCADE,related_name='user')
    role = models.ForeignKey(Roles,on_delete = models.CASCADE,related_name='role')

    class Meta:
        managed = True
        ordering = ['role']

